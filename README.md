# fantom-concole

Console application computes and verifies a Boneh-Lynn-Shacham signature in a simulated conversation between Alice & Bob

tar -xvf pbc-0.5.14.tar.gz  

sudo apt-get install libgmp-dev sudo ldconfig 

sudo apt-get install build-essential flex bison

./configure

make 

sudo make install 

sudo ldconfig 

For test: 

go build 1S.go 

./1S 


Docker:

docker pull ibisolutions/fantom 

start:

docker run ibisolutions/fantom 1S
