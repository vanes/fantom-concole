FROM ubuntu:latest
RUN apt-get update
RUN apt-get install libgmp-dev -y
RUN apt-get install build-essential flex bison -y

WORKDIR /home

ADD . /home

RUN ./configure
RUN make
RUN make install
RUN ldconfig

RUN apt-get install  golang-go -y
RUN mkdir /usr/lib/go-1.10/src/pbc
RUN cp /home/pbc-src/*.* /usr/lib/go-1.10/src/pbc  
RUN go build 1S.go
RUN ./1S
RUN ln /home/1S /usr/bin/1S
EXPOSE 80 
CMD ["/usr/bin/1S"]
